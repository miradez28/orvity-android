// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

export const firebaseConfig = {
  apiKey: "AIzaSyCCLWi9SUkycbLcF_Z8e-L3wND9BVQj9uM",
  authDomain: "orvity-c5c4b.firebaseapp.com",
  projectId: "orvity-c5c4b",
  storageBucket: "orvity-c5c4b.appspot.com",
  messagingSenderId: "2801691076",
  appId: "1:2801691076:web:c993332ec23c8d11675c91",
  measurementId: "G-Z17XTDKTR6"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
