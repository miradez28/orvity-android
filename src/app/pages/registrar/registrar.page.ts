import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.page.html',
  styleUrls: ['./registrar.page.scss'],
})
export class RegistrarPage implements OnInit {

  @ViewChild('verificarcontra', { read: ElementRef }) verificarcontra: ElementRef;

  @ViewChild('repetirverificarcontra', { read: ElementRef }) repetirverificarcontra: ElementRef;

  vertipo = false;

  myphoto: any;
  imagenvacia1 = '';
  imagenvacia = '';
  nombre = '';
  apellidos = '';
  email = '';
  messenger = '';
  whatsapp = '';
  contras = '';
  contrasCon = '';
  terminos = false;

  sucursal = '';
  razon_social = '';
  rfc = '';
  phone='';
  giro = '';
  Actividad_Empresa_idAct = '';
  Actividad_Empresa_idAct1 = '';
  pais = '';
  estado = '';

  PaisS: any = [];
  EstadosS: any = [];
  EmpresaActi: any = [];

  repetirtipoverificarcontra  =  'password';
  tipoverificarcontra  =  'password';

  emailerror = false;

  constructor(
    private http:HttpClient
  ) {

   }

  ngOnInit() {
    this.metodoObtenerActividades();
    this.metodoObtenerPais();
  }

  ionViewWillEnter() {
    this.limpiarvariables();
  }

  tipoderegistro(){
    if(this.vertipo === false) {
      this.vertipo = true;
    } else {
      this.vertipo = false;
    }
  }

  limpiarvariables(){
    this.nombre = '';
    this.apellidos = '';
    this.email = '';
    this.messenger = '';
    this.whatsapp = '';
    this.contras = '';
    this.contrasCon = '';
    this.terminos = false;
    this.sucursal = '';
    this.razon_social = '';
    this.rfc = '';
    this.phone='';
    this.giro = '';
    this.Actividad_Empresa_idAct = '';
    this.pais = '';
    this.estado = '';
    this.emailerror = false;
  }

  metodoObtenerActividades() {

    this.http.get('assets/data/actividades.json').subscribe(Response => {
      this.EmpresaActi = Response['actividades'];
    });

  }

  metodoObtenerPais() {
    var arraypaises = [];

    this.http.get('assets/data/paises.json').subscribe(Response => {
      arraypaises = Response['paises'];
      arraypaises.sort(function(a, b){ return a.name > b.name ? 1 : b.name > a.name ? -1 : 0 });
      this.PaisS = arraypaises;
    });

  }

  metodoObtenerEstados(pais) {

    var arrayestados = [];
    var arrayestadosrecorrido = [];

    this.http.get('assets/data/estados.json').subscribe(Response => {
      arrayestados = Response['estados'];
      for (let i = 0; i < arrayestados.length; i++) {
        if (arrayestados[i].id_country === pais) {
          arrayestadosrecorrido.push(arrayestados[i]);
        }
      }
      this.EstadosS = arrayestadosrecorrido;
    });

  }

  obteneractividad(event: { component: IonicSelectableComponent,value: any}) {
    console.log('actividad:', event.value);
    this.giro = event.value.idAct;
    this.Actividad_Empresa_idAct1 = '';
    console.log(this.giro);
  }

  obtenerpaises(event: { component: IonicSelectableComponent,value: any}) {
    console.log('pais:', event.value);
    this.pais = event.value.id;
    console.log(this.pais);
    this.estado = '';
    this.metodoObtenerEstados(this.pais);
  }

  obtenerestado(event: { component: IonicSelectableComponent,value: any}) {
    console.log('pais:', event.value);
    this.estado = event.value.id;
    console.log(this.estado);
  }

  vercontra() {

    this.tipoverificarcontra = this.tipoverificarcontra === 'text' ? 'password' : 'text';
    const nativeEl = this.verificarcontra.nativeElement.querySelector('input');
    const inputSelection = nativeEl.selectionStart;
    nativeEl.focus();
    setTimeout(() => {
         nativeEl.setSelectionRange(inputSelection, inputSelection);
     }, 1);

  }

  vercontrarepetir() {

    this.repetirtipoverificarcontra = this.repetirtipoverificarcontra === 'text' ? 'password' : 'text';
    const nativeEl = this.repetirverificarcontra.nativeElement.querySelector('input');
    const inputSelection = nativeEl.selectionStart;
    nativeEl.focus();
    setTimeout(() => {
         nativeEl.setSelectionRange(inputSelection, inputSelection);
     }, 1);

  }

  correovalido(e){
    var verificar = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (verificar.test(this.email)){
      this.emailerror = false;
     } else {
      this.emailerror = true;
     }
  }

}
