import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild('vercontra') vercontra;

  correo = '';
  contra = '';
  tipodetexto  =  'password';
  iconpassword  =  'eye-off';

  constructor() { }

  ngOnInit() {
  }

  logForm() {
  }

  Espaciosdx(event) {
    let contenido = event.target.value;
    event.target.value = contenido.replace(" ", "");
  }

  metodovercontra() {
    this.tipodetexto  =  this.tipodetexto  ===  'text'  ?  'password'  :  'text';
    this.iconpassword  =  this.iconpassword  ===  'eye-off'  ?  'eye'  :  'eye-off';
    this.vercontra.el.setFocus();
}

}
